import {parser} from '../functions/parserFunctions.js'

describe('Parser runinig', () =>{
    it('Should return 0 becaouse is an empty string', () => {
        expect(parser("")).toBe("0");
    });

    it('Should return 0 0 0 0 0 becaouse is a string with only spaces', () => {
        expect(parser("    ")).toBe("0 0 0 0 0");
    });

    test('If can parse a string with only symbols and spaces', () => {
    	expect(parser("#@#$ @#$@#$#@")).toBe("0#0@0#0$0 0@0#0$0@0#0$0#0@0");
    })

    test('If can parse a normal string with only spaces', () => {
    	expect(parser("This is a test string")).toBe("T2s i0s a0a t2t s4g");
    })

    test('If can parse a normal string with spaces and symbols at the begining', () => {
    	expect(parser("@$  Hi$my7name%is#Erik_and i'am}from*Mexico")).toBe("0@0$0 0 H0i$m0y7n2e%i0s#E2k_a1d i0i'a0m}f2m*M4o");
    })
    test('If can parse a normal string with spaces and symbols at the begining and end', () => {
    	expect(parser("@$  Hi$my7name%is#Erik_and i'am}from*Mexico!@# !@#@!")).toBe("0@0$0 0 H0i$m0y7n2e%i0s#E2k_a1d i0i'a0m}f2m*M4o!0@0#0 0!0@0#0@0!0");
    })
});
