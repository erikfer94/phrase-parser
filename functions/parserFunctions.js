export const parser = (s) => {
  if(s.length === 0){
    return "0"
  }
  const words = s.split(/[^a-zA-Z]/g);
  const symbols = s.match(/[^a-zA-Z]/g);
  const size = words.length;
  for( let i = 0; i < size; i++){
    let word = words[i];
    let sizeWord = word.length;
    if(sizeWord === 0){
      words[i] = "0"
    }
    else{
      let firstLetter = word[0];
      let lastLetter = word[sizeWord - 1]
      words[i] = firstLetter + counter(word) + lastLetter
    }
  }
  return pastePhrase(words, symbols)
}

const counter = (w) => {
  let size = w.length - 1;
  if(size < 2){
    return 0
  }
  const letters = [];
  let counter = 0;
  for(let k = 1; k < size; k ++){
    if(!letters.includes(w[k])){
      letters.push(w[k]);
      counter++
    }
  }
  return counter
}

const pastePhrase = (words, symbols) => {
  const size = words.length;
  let newPhrase = ""
  for( let i = 0; i < size; i++){
    newPhrase += words[i] + (symbols[i] || "")
  }

  return newPhrase;
}

